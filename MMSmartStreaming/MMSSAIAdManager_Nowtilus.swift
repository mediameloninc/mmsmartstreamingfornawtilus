//
//  MMSSAIAdManager_Nowtilus.swift
//  BasicPlayback
//
//  Created by MacAir 1 on 14/09/20.
//  Copyright © 2020 Bitmovin. All rights reserved.
//

import UIKit
import BitmovinPlayer
import MMSmartStreamingPrivate

protocol MMSSAIDelegate {
    func notifyAdEventWith(state: MMAdState, vastAd: VastAd?, vastTracker: VastTracker, andAdIndex index: Int)
}

class MMSSAIAdManager_Nowtilus: NSObject {
    //MARK:- OBJECTS
    @objc public var isAdManagerSet = false
    public var delegate: MMSSAIDelegate?
    private var stringMediaURL: String!
    private var stringVastURL: String!
    private var manifestTargetDuration = 0
    private var currentAdStartTime = 0
    private var currentAdEndTime = 0
    private var vastClient = VastClient()
    private var vastTracker: VastTracker?
    private var fakePlayheadProgressTimer: Timer?
    private var playhead = 0.0
    private var player: BitmovinPlayer!
    private var isLogTraceEnabled = true
    private var timerGetVastResponse: Timer?
    private var timeDelayToCheckVastResponse: Double = 0.0
    private var isAdStarted = false
    private var adIndex = 0
    private var isAdRequested = false

    //MARK:- INIT
    override init() {
        super.init()
    }
        
    @objc public init(_player: BitmovinPlayer) {
        super.init()
        self.player = _player
        //self.playhead = self.player.currentTime
    }
        
    //MARK:- SET REDIRECTED URL
    @objc public func setupMediaURL(mediaURL: String) {
        self.stringMediaURL = mediaURL
        self.isAdManagerSet = true
        self.parseManifestURL(mediaURL: self.stringMediaURL)
    }
        
    @objc public func notifyPlayerPlaybackTime(playbackTime: Int) {
        self.playhead = Double(playbackTime)
        guard let tracker = vastTracker else { return }
        do {
            if self.currentAdEndTime <= Int(self.playhead) && self.isAdStarted {
                try tracker.trackAdComplete()
                self.adComplete(vastTracker: tracker, ad: tracker.vastModel.ads[self.adIndex])
                self.isAdStarted = false
                self.adIndex = self.adIndex + 1
                if (self.adIndex == tracker.vastModel.ads.count - 1) {
                    self.adBreakComplete(vastTracker: tracker)
                }
            } else if Int(self.playhead / 1000) > (self.currentAdStartTime / 1000) && !self.isAdStarted {
                if (self.adIndex < tracker.vastModel.ads.count) {
                    self.isAdStarted = true
                    if (self.adIndex == 0) {
                        self.adBreakStart(vastTracker: tracker)
                    }
                    try tracker.trackAdStart(withId: tracker.vastModel.ads[self.adIndex].id)
                    self.adStart(vastTracker: tracker, ad: tracker.vastModel.ads[self.adIndex])
                }
            } else {
                try tracker.updateProgress(time: self.playhead / 1000)
            }
        } catch TrackingError.unableToUpdateProgress(msg: let msg) {
            print("Tracking Error > Unable to update progress: \(msg)")
            if (self.adIndex == tracker.vastModel.ads.count - 1) {
                self.adBreakComplete(vastTracker: tracker)
            }
        } catch {
            print("Tracking Error > unknown")
            if (self.adIndex == tracker.vastModel.ads.count - 1) {
                self.adBreakComplete(vastTracker: tracker)
            }
        }
    }
        
    //MARK:- LOG MESSAGE
    private func mmSSAILogger(message: String) {
        if (self.isLogTraceEnabled && !message.isEmpty) {
            print("MM SSAI LOG: ==> \(message)")
        }
    }
        
    //MARK:- PARSE MANIFEST URL
    private func parseManifestURL(mediaURL: String) {
        do {
            let m3u8PlaylistModel = try M3U8PlaylistModel(url: URL(string: mediaURL)!)
            let stringVarientManifestXML = m3u8PlaylistModel.mainMediaPl.originalText
            self.stringVastURL = (m3u8PlaylistModel.masterPlaylist.allStreamURLs()?.first as! URL).absoluteString + "/vast"
            if let arrayTags = stringVarientManifestXML?.components(separatedBy: "\n") {
                for tag in arrayTags {
                    if tag.contains("#EXT-X-PROGRAM-DATE-TIME") {
                        if let stringPDT = tag.components(separatedBy: "#EXT-X-PROGRAM-DATE-TIME:").last {
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                            if let programDate = dateFormatter.date(from: stringPDT) {
                                let seconds = programDate.timeIntervalSince1970
                                let pdt = Int(seconds * 1000)
                            }
                        }
                    } else if tag.contains("#EXT-X-TARGETDURATION") {
                        if let targetDuration = tag.components(separatedBy: "#EXT-X-TARGETDURATION:").last {
                            self.manifestTargetDuration = Int(targetDuration)!
                            self.timeDelayToCheckVastResponse = Double(targetDuration)! / 2
                            self.invalidateTimer()
                            self.activateTimer()
                            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(self.manifestTargetDuration)) {
                                self.parseManifestURL(mediaURL: self.stringMediaURL)
                            }
                        }
                    }
                }
            }
        } catch {
            print("Could not able to get manifest url")
        }
    }
        
    //MARK:- PARSE VAST URL
    @objc private func parseVASTURL() {
        let request = URLRequest(url: URL(string: self.stringVastURL)!)
        let session = URLSession(configuration: URLSessionConfiguration.default)

        let task = session.dataTask(with: request) {(data, response, error) in
            DispatchQueue.global().async {
                if error != nil || data == nil {
                    print("Client error!")
                    return
                }

                guard let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode) else {
                    print("Server error!")
                    return
                }

                guard let mime = response.mimeType, mime == "application/json" else {
                    print("Wrong MIME type!")
                    return
                }
                
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
                    if let jsonDict = json {
                        if let adStartTime = jsonDict["time"] as? NSNumber {
                            if self.currentAdStartTime == Int(truncating: adStartTime) {
                                return
                            }
                            self.currentAdStartTime = Int(truncating: adStartTime)
                            self.adIndex = 0
                        }
                        if let dataString = jsonDict["vast"] as? String {
                            self.makeVastRequestWithXMLData(stringXMLData: dataString)
                        }
                    }
                } catch {
                    print("parseVASTURL JSON error: \(error.localizedDescription)")
                }
                print("parseVASTURL Completed request: \(request.debugDescription)")
            }
        }

        task.resume()
    }
        
    //MARK:- MAKE VAST REQUEST WITH XML DATA
    private func makeVastRequestWithXMLData(stringXMLData: String) {
        self.vastClient.parseVast(withContentsOf: stringXMLData) { (vastModel, error) in
            if let error = error as? VastError {
                switch error {
                case .invalidXMLDocument:
                    print("Error: Invalid XML document")
                case .invalidVASTDocument:
                    print("Error: Invalid Vast Document")
                case .unableToCreateXMLParser:
                    print("Error: Unable to Create XML Parser")
                case .unableToParseDocument:
                    print("Error: Unable to Parse Vast Document")
                default:
                    print("Error: unexpected error ...")
                }
                return
            }
                
            guard let vastModel = vastModel else {
                print("Error: unexpected error ...")
                return
            }
                
            self.trackVastAd(from: vastModel)
        }
    }
        
    //MARK:- TRACK VAST AD
    private func trackVastAd(from vastModel: VastModel) {
        self.vastTracker = VastTracker(vastModel: vastModel, startTime: Double(self.currentAdStartTime / 1000), supportAdBuffets: true, delegate: self, trackProgressCumulatively: true)
    }
        
    //MARK:- TIMER FUNCTIONS
    private func activateTimer() {
        if self.timeDelayToCheckVastResponse > 0.0 {
            self.timerGetVastResponse = Timer.scheduledTimer(timeInterval: self.timeDelayToCheckVastResponse, target: self, selector: #selector(self.parseVASTURL), userInfo: nil, repeats: true)
            self.timerGetVastResponse!.fire()
        }
    }
    private func invalidateTimer() {
        if let timer = self.timerGetVastResponse {
            timer.invalidate()
        }
    }
}

//MARK:- VAST TRACKER DELEGATE METHODS
extension MMSSAIAdManager_Nowtilus: VastTrackerDelegate {
    func adBreakStart(vastTracker: VastTracker) {
        self.delegate?.notifyAdEventWith(state: MMAdState.AD_BREAK_STARTED, vastAd: nil, vastTracker: vastTracker, andAdIndex: 0)
    }
    
    func adStart(vastTracker: VastTracker, ad: VastAd) {
        let duration = ad.creatives.first?.linear?.duration
        self.currentAdEndTime = Int(playhead) + Int((duration ?? 0)) * 1000
        
        if !self.isAdRequested {
            self.delegate?.notifyAdEventWith(state: MMAdState.AD_REQUEST, vastAd: ad, vastTracker: vastTracker, andAdIndex: self.adIndex)
            self.isAdRequested = true
        }
        
        self.delegate?.notifyAdEventWith(state: MMAdState.AD_IMPRESSION, vastAd: ad, vastTracker: vastTracker, andAdIndex: self.adIndex)
        
        self.delegate?.notifyAdEventWith(state: MMAdState.AD_STARTED, vastAd: ad, vastTracker: vastTracker, andAdIndex: self.adIndex)
    }
    
    func adFirstQuartile(vastTracker: VastTracker, ad: VastAd) {
        self.delegate?.notifyAdEventWith(state: MMAdState.AD_FIRST_QUARTILE, vastAd: ad, vastTracker: vastTracker, andAdIndex: self.adIndex)
    }
    
    func adMidpoint(vastTracker: VastTracker, ad: VastAd) {
        self.delegate?.notifyAdEventWith(state: MMAdState.AD_MIDPOINT, vastAd: ad, vastTracker: vastTracker, andAdIndex: self.adIndex)
    }
    
    func adThirdQuartile(vastTracker: VastTracker, ad: VastAd) {
        self.delegate?.notifyAdEventWith(state: MMAdState.AD_THIRD_QUARTILE, vastAd: ad, vastTracker: vastTracker, andAdIndex: self.adIndex)
    }
    
    func adComplete(vastTracker: VastTracker, ad: VastAd) {
        self.delegate?.notifyAdEventWith(state: MMAdState.AD_COMPLETED, vastAd: ad, vastTracker: vastTracker, andAdIndex: self.adIndex)
    }
    
    func adBreakComplete(vastTracker: VastTracker) {
        self.delegate?.notifyAdEventWith(state: MMAdState.AD_BREAK_COMPLETED, vastAd: nil, vastTracker: vastTracker, andAdIndex: self.adIndex)
    }
}
