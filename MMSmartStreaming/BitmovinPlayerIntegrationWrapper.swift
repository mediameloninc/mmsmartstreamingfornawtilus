import UIKit
import CoreTelephony
import UIKit
import AVFoundation
import BitmovinPlayer
#if os(iOS)
  import CoreTelephony
#endif
import MMSmartStreamingPrivate

@objc public protocol SSAIManagerDelegate {
    @objc func notifyAdEventsWith(eventName: MMAdState, andAdInfo adInfo: MMAdInfo)
}

@objc public class BitmovinPlayerIntegrationWrapper: NSObject, MMSmartStreamingObserver {
    //MARK:- OBJECTS
    enum MMCurrentPlayerState {
        case IDLE,
        PLAYING,
        PAUSED,
        STOPPED,
        ERROR
    }
    private var timer:Timer?
    weak fileprivate var player:BitmovinPlayer?
    private var delegate: BitMovinPlayerDelegate?
    fileprivate var mmssaiAdManager: MMSSAIAdManager_Nowtilus?
    private var presentationDuration: TimeInterval?
    private var lastReportedBitrate = 0
    fileprivate let TIME_INCREMENT = 2.0
    private var presentationInfoSet = false
    fileprivate var currentState = MMCurrentPlayerState.IDLE
    fileprivate var currentAdState = MMAdState.UNKNOWN
    var assetInfo:MMAssetInformation?
    private static var enableLogging = false
    lazy var mmSmartStreaming:MMSmartStreaming = MMSmartStreaming.getInstance() as! MMSmartStreaming
    private static var appNotificationObsRegistered = false
    private var isAdStreaming = false
    private var isAdBuffering = false
    private var isPostRollAd = false
    private var adDuration = 0
    var mmssaiDelegate: SSAIManagerDelegate?
    fileprivate var isOnloaded = false
    private var shouldInitSession = false
    
    /*
     * Singleton instance of the adaptor
     */
    public static let shared = BitmovinPlayerIntegrationWrapper()
    
    /**
     * Gets the version of the SDK
     */
    @objc public static func getVersion() -> String! {
        return "0.0.1/\(String(describing: MMSmartStreaming.getVersion()!))"
    }
    
    /**
     * If for some reasons, accessing the content manifest by SDK interferes with the playback. Then user can disable the manifest fetch by the SDK.
     * For example - If there is some token scheme in content URL, that makes content to be accessed only once, then user of SDK may will like to call this API.
     * So that player can fetch the manifest
     */
    @objc public static func disableManifestsFetch(disable: Bool) {
        return MMSmartStreaming.disableManifestsFetch(disable)
    }
    
    /**
     * Allows user of SDK to provide information on Customer, Subscriber and Player to the SDK
     * Please note that it is sufficient to call this API only once for the lifetime of the application, and all playback sessions will reuse this information.
     *
     * Note - User may opt to call initializeAssetForPlayer instead of calling this API, and provide the registration information in its param every time as they provide the asset info. This might help ease the integration.
     *
     * This API doesnt involve any network IO, and is very light weight. So calling it multiple times is not expensive
     */
    public static func setPlayerRegistrationInformation(registrationInformation pInfo: MMRegistrationInformation?, player aPlayer: BitmovinPlayer?) {
        if let pInfo = pInfo {
            BitmovinPlayerIntegrationWrapper.logDebugStatement("=============setPlayerInformation - pInfo=============")
            #if os(iOS)
                pInfo.setComponentName("IOSSDK")
            #elseif os(tvOS)
                pInfo.setComponentName("tvOSSDK")
            #endif
            
            BitmovinPlayerIntegrationWrapper.registerMMSmartStreaming(playerName: pInfo.playerName, custID: pInfo.customerID, component: pInfo.component, subscriberID: pInfo.subscriberID, domainName: pInfo.domainName, subscriberType: pInfo.subscriberType, subscriberTag: pInfo.subscriberTag)
            BitmovinPlayerIntegrationWrapper.reportPlayerInfo(brand: pInfo.playerBrand, model: pInfo.playerModel, version: pInfo.playerVersion)
        }
        if let oldPlayer = BitmovinPlayerIntegrationWrapper.shared.player {
            BitmovinPlayerIntegrationWrapper.shared.cleanupSession(player: oldPlayer)
        }
        if let newPlayer = aPlayer {
            BitmovinPlayerIntegrationWrapper.shared.createSession(player: newPlayer)
        }
    }

    /**
     * Application may create the player with the AVAsset for every session they do the playback
     * User of API must provide the asset Information
     */
    @objc public static func initializeAssetForPlayer(assetInfo aInfo: MMAssetInformation, registrationInformation pInfo: MMRegistrationInformation?, player aPlayer: BitmovinPlayer?) {
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        BitmovinPlayerIntegrationWrapper.logDebugStatement("================initializeAssetForPlayer \(aInfo.assetURL)=============")
        BitmovinPlayerIntegrationWrapper.setPlayerRegistrationInformation(registrationInformation: pInfo, player:aPlayer)
        BitmovinPlayerIntegrationWrapper.changeAssetForPlayer(assetInfo: aInfo, player: aPlayer)
    }

    /**
     * Whenever the asset with the player is changed, user of the API may call this API
     * Please note either changeAssetForPlayer or initializeAssetForPlayer should be called
     */
    public static func changeAssetForPlayer(assetInfo aInfo: MMAssetInformation, player aPlayer: BitmovinPlayer?) {
        BitmovinPlayerIntegrationWrapper.logDebugStatement("================changeAssetForPlayer \(aInfo.assetURL)=============")
        BitmovinPlayerIntegrationWrapper.shared.assetInfo = aInfo
        BitmovinPlayerIntegrationWrapper.shared.cleanupCurrItem();

        if let newPlayer = aPlayer {
            if let oldPlayer = BitmovinPlayerIntegrationWrapper.shared.player {
                if oldPlayer != newPlayer {
                    BitmovinPlayerIntegrationWrapper.shared.cleanupSession(player: oldPlayer)
                    BitmovinPlayerIntegrationWrapper.shared.player = nil;
                    BitmovinPlayerIntegrationWrapper.shared.createSession(player: newPlayer)
                }
            }
            BitmovinPlayerIntegrationWrapper.shared.shouldInitSession = true
        }
    }
    
    fileprivate static func setMediaURLInAssetInfo(urlString: String) {
        guard let _ = BitmovinPlayerIntegrationWrapper.shared.assetInfo else {
            return
        }
        BitmovinPlayerIntegrationWrapper.shared.assetInfo!.assetURL = urlString
        if BitmovinPlayerIntegrationWrapper.shared.shouldInitSession {
            BitmovinPlayerIntegrationWrapper.shared.shouldInitSession = false
            BitmovinPlayerIntegrationWrapper.shared.initSession(deep: true)
        }
    }
    
    /**
    * Whenever the asset with the player is updated, user of the API may call this API
    */
    @objc public static func updateAssetInfo(assetInfo aInfo: MMAssetInformation) {
        if let player = BitmovinPlayerIntegrationWrapper.shared.player {
            BitmovinPlayerIntegrationWrapper.shared.cleanupCurrItem()
            self.changeAssetForPlayer(assetInfo: aInfo, player: player)
            BitmovinPlayerIntegrationWrapper.shared.setPresentationInformationForContent()
            BitmovinPlayerIntegrationWrapper.shared.mmSmartStreaming.report(MMPlayerState.PLAYING)
            BitmovinPlayerIntegrationWrapper.shared.currentState = .PLAYING
        }
    }

    /**
     * Once the player is done with the playback session, then application should call this API to clean up observors set with the player and the player's current item
     */
    @objc public static func cleanUp() {
        BitmovinPlayerIntegrationWrapper.logDebugStatement("================cleanUp=============")
        BitmovinPlayerIntegrationWrapper.shared.cleanupInstance()
    }

    /**
     * Application may update the subscriber information once it is set via MMRegistrationInformation
     */
    public static func updateSubscriber(subscriberId: String!, subscriberType: String!, subscriberMetadata: String!) {
        MMSmartStreaming.updateSubscriber(withID: subscriberId, andType: subscriberType, withTag:subscriberMetadata)
    }

    /**
     * Application may report the custom metadata associated with the content using this API.
     * Application can set it as a part of MMAVAssetInformation before the start of playback, and
     * can use this API to set metadata during the course of the playback.
     */
    public func reportCustomMetadata(key: String!, value: String!) {
        mmSmartStreaming.reportCustomMetadata(withKey: key, andValue: value)
    }

    /**
     * Used for debugging purposes, to enable the log trace
     */
    public func enableLogTrace(logStTrace: Bool) {
        BitmovinPlayerIntegrationWrapper.enableLogging = logStTrace
        mmSmartStreaming.enableLogTrace(logStTrace)
    }

    /**
     * If application wants to send application specific error information to SDK, the application can use this API.
     * Note - SDK internally takes care of notifying the error messages provided to it by AVFoundation framwork
     */
    public func reportError(error: String, playbackPosMilliSec:Int) {
        mmSmartStreaming.reportError(error, atPosition: playbackPosMilliSec)
    }

    public static func reportMetricValue(metricToOverride: MMOverridableMetrics, value: String!) {
        switch metricToOverride {
        case MMOverridableMetrics.CDN:
            BitmovinPlayerIntegrationWrapper.shared.mmSmartStreaming.reportMetricValue(for: MMOverridableMetric.ServerAddress, value: value)
        default:
            print("Only CDN metric is overridable as of now ...")
        }
    }

    private static func logDebugStatement(_ logStatement: String) {
        if(BitmovinPlayerIntegrationWrapper.enableLogging) {
            //os_log("mediamelon.smartstreaming.avplayer-ios %{public}@", logStatement)
            NSLog("%s", logStatement)
        }
    }

    private static func registerMMSmartStreaming(playerName: String, custID: String, component: String,  subscriberID: String?, domainName: String?, subscriberType: String?, subscriberTag: String?) {
        MMSmartStreaming.registerForPlayer(withName: playerName, forCustID: custID, component: component, subsID: subscriberID, domainName: domainName, andSubscriberType: subscriberType, withTag:subscriberTag)
        
        var operatorName = ""
        var osName = ""
        var osVersion = UIDevice.current.systemVersion
        let brand = "Apple"
        let model = UIDevice.current.model
        let screenWidth = Int(UIScreen.main.bounds.width)
        let screenHeight = Int(UIScreen.main.bounds.height)
        
        #if os(iOS)
            let phoneInfo = CTTelephonyNetworkInfo()
            if #available(iOS 12.0, *) {
                if let carrier = phoneInfo.serviceSubscriberCellularProviders, let dict = carrier.first, let opName = dict.value.carrierName {
                    operatorName = opName
                }
            }
            else {
                if let carrierName = phoneInfo.subscriberCellularProvider?.carrierName{
                    operatorName = carrierName
                }
            }
            osName = "iOS"
            MMSmartStreaming.reportDeviceInfo(withBrandName: brand, deviceModel: model, osName: osName, osVersion: osVersion, telOperator: operatorName, screenWidth: screenWidth, screenHeight: screenHeight, andType: model)
        #elseif os(tvOS)
            osName = "tvOS"
            MMSmartStreaming.reportDeviceInfo(withBrandName: brand, deviceModel: model, osName: osName, osVersion: osVersion, telOperator: operatorName, screenWidth: screenWidth, screenHeight: screenHeight, andType:"AppleTV")
        #endif
    }

    private static func reportPlayerInfo(brand: String?, model: String?, version: String?) {
        MMSmartStreaming.reportPlayerInfo(withBrandName: brand, model: model, andVersion: version)
    }

    private func initializeSession(mode: MMQBRMode!, manifestURL: String!, metaURL: String?, assetID: String?, assetName: String?, videoId: String?) {
        var connectionInfo:MMConnectionInfo!
        let reachability = ReachabilityMM()

        #if os(iOS)
        func getDetailedMobileNetworkType() {
            let phoneInfo = CTTelephonyNetworkInfo()
            if #available(iOS 12.0, *) {
                if let arrayNetworks = phoneInfo.serviceCurrentRadioAccessTechnology {
                    var networkString = ""
                    for value in arrayNetworks.values {
                        networkString = value
                    }
                    if networkString == CTRadioAccessTechnologyLTE{
                        connectionInfo = .cellular_4G
                    }else if networkString == CTRadioAccessTechnologyWCDMA{
                        connectionInfo = .cellular_3G
                    }else if networkString == CTRadioAccessTechnologyEdge{
                        connectionInfo = .cellular_2G
                    }
                }
            } else {
                let networkString = phoneInfo.currentRadioAccessTechnology
                if networkString == CTRadioAccessTechnologyLTE{
                    connectionInfo = .cellular_4G
                }else if networkString == CTRadioAccessTechnologyWCDMA{
                    connectionInfo = .cellular_3G
                }else if networkString == CTRadioAccessTechnologyEdge{
                    connectionInfo = .cellular_2G
                }
            }
        }
        #endif

        if let reachability = reachability{
            do{
                try reachability.startNotifier()
                let status = reachability.currentReachabilityStatus
                
                if(status == .notReachable){
                    connectionInfo = .notReachable
                }
                else if (status == .reachableViaWiFi){
                    connectionInfo = .wifi
                }
                else if (status == .reachableViaWWAN){
                    connectionInfo = .cellular
                    #if os(iOS)
                        getDetailedMobileNetworkType()
                    #endif
                }
            }
            catch{
                
            }
        }
        if (connectionInfo != nil) {
            self.mmSmartStreaming.reportNetworkType(connectionInfo)
        }
        mmSmartStreaming.initializeSession(with: mode, forManifest: manifestURL, metaURL: metaURL, assetID: assetID, assetName: assetName, videoID:videoId, for: self)
    }

    func reportNetworkType(connInfo: MMConnectionInfo) {
        mmSmartStreaming.reportNetworkType(connInfo)
    }

    private func reportLocation(latitude: Double, longitude: Double) {
        mmSmartStreaming.reportLocation(withLatitude: latitude, andLongitude: longitude)
    }

    public func sessionInitializationCompleted(with status: MMSmartStreamingInitializationStatus, andDescription description: String?, forCmdWithId cmdId: Int) {
        BitmovinPlayerIntegrationWrapper.logDebugStatement("sessionInitializationCompleted - status \(status) description \(String(describing: description))")
    }

    private func reportChunkRequest(chunkInfo: MMChunkInformation!) {
        mmSmartStreaming.reportChunkRequest(chunkInfo)
    }

    private func setPresentationInformation(presentationInfo: MMPresentationInfo!) {
        mmSmartStreaming.setPresentationInformation(presentationInfo)
    }

    private func reportDownloadRate(downloadRate: Int!) {
        mmSmartStreaming.reportDownloadRate(downloadRate)
    }

    fileprivate func reportBufferingStarted() {
        mmSmartStreaming.reportBufferingStarted()
    }

    fileprivate func reportBufferingCompleted() {
        mmSmartStreaming.reportBufferingCompleted()
    }

    fileprivate func reportABRSwitch(prevBitrate: Int, newBitrate: Int) {
        mmSmartStreaming.reportABRSwitch(fromBitrate: prevBitrate, toBitrate: newBitrate)
    }

    private func reportFrameLoss(lossCnt: Int) {
        mmSmartStreaming.reportFrameLoss(lossCnt)
    }

    @objc private func timeoutOccurred() {
        guard self.player != nil else {
            return
        }
        mmSmartStreaming.reportPlaybackPosition(getPlaybackPosition())
        
        if let videoQuality = self.player!.videoQuality {
            if (self.lastReportedBitrate != Int(videoQuality.bitrate)) {
                if (self.lastReportedBitrate != 0) {
                    //self.reportABRSwitch(prevBitrate: self.lastReportedBitrate, newBitrate: Int(videoQuality.bitrate))
                }
                self.reportDownloadRate(downloadRate: Int(videoQuality.bitrate))
                self.lastReportedBitrate = Int(videoQuality.bitrate)
            }
        }
    }

    fileprivate func reportPresentationSize(width: Int, height: Int){
        mmSmartStreaming.reportPresentationSize(withWidth: width, andHeight: height)
    }

    fileprivate func getPlaybackPosition() -> Int {
        guard let player = player else{
            return 0;
        }
        let time = player.currentTime * 1000
        if (time > 0) {
            return Int(time) * 1000
        } else {
            return 0
        }
    }
    
    private func cleanupInstance() {
        self.cleanupCurrItem()
        cleanupSession(player: self.player)
    }
    
    fileprivate func cleanupCurrItem(){
        guard let _ = self.player else {
            return;
        }
        self.resetSession()
    }
    
    private func resetSession(){
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        
        if let tmr = self.timer{
            tmr.invalidate();
        }
        self.reportStoppedState()
    }

    private func handleErrorWithMessage(message: String?, error: Error? = nil) {
        BitmovinPlayerIntegrationWrapper.logDebugStatement("--- Error occurred with message: \(String(describing: message)), error: \(String(describing: error)).")
        mmSmartStreaming.reportError(String(describing: error), atPosition: getPlaybackPosition())
    }

    private func reset() {
        self.presentationInfoSet = false
        self.currentState = MMCurrentPlayerState.IDLE
        self.isOnloaded = false

        if BitmovinPlayerIntegrationWrapper.appNotificationObsRegistered == false {
            BitmovinPlayerIntegrationWrapper.appNotificationObsRegistered = true

            NotificationCenter.default.addObserver (
                forName: NSNotification.Name.UIApplicationWillResignActive,
                object: nil,
                queue: nil) { (notification) in
                    ReachabilityManager.shared.stopMonitoring()
            }

            NotificationCenter.default.addObserver (
                forName: NSNotification.Name.UIApplicationDidBecomeActive,
                object: nil,
                queue: nil) { (notification) in
                    ReachabilityManager.shared.startMonitoring()
            }
            ReachabilityManager.shared.startMonitoring()
        }
    }

    private func createSession(player: BitmovinPlayer) {
        BitmovinPlayerIntegrationWrapper.logDebugStatement("*** createSession")
        self.player = player
        self.mmssaiAdManager = MMSSAIAdManager_Nowtilus(_player: player)
        self.mmssaiAdManager!.delegate = self
        self.addPlayerListener()
        timer = nil
    }

    private func initSession(deep: Bool) {
        BitmovinPlayerIntegrationWrapper.logDebugStatement("*** initSession")
        
        if(deep) {
            reset()
        }

        guard let assetInfo = self.assetInfo else {
            BitmovinPlayerIntegrationWrapper.logDebugStatement("!!! Error - assetInfo not set !!!")
            return
        }

        if(deep) {
            BitmovinPlayerIntegrationWrapper.shared.initializeSession(mode: assetInfo.qbrMode, manifestURL: assetInfo.assetURL, metaURL: assetInfo.metafileURL? .absoluteString, assetID: assetInfo.assetID, assetName: assetInfo.assetName, videoId: assetInfo.videoId)
            for (key, value) in assetInfo.customKVPs {
                BitmovinPlayerIntegrationWrapper.shared.reportCustomMetadata(key: key, value: value)
            }
        }else{
            for (key, value) in assetInfo.customKVPs {
                BitmovinPlayerIntegrationWrapper.shared.reportCustomMetadata(key: key, value: value)
            }
        }

        self.timer = Timer.scheduledTimer(timeInterval: self.TIME_INCREMENT, target:self, selector:#selector(BitmovinPlayerIntegrationWrapper.timeoutOccurred), userInfo: nil, repeats: true)

        let url = assetInfo.assetURL
        BitmovinPlayerIntegrationWrapper.logDebugStatement("Initializing for \(String(describing: url))")
    }


    private func cleanupSession(player: BitmovinPlayer?) {
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        if let bmPlayer = player {
            self.removePlayerListener(player: bmPlayer)
        }
        self.player = nil
        self.mmssaiAdManager = nil
        BitmovinPlayerIntegrationWrapper.logDebugStatement("removeSession ***")
    }

    private func continueStoppedSession() {
        guard let _ = self.player else {
            BitmovinPlayerIntegrationWrapper.logDebugStatement("!!! Error - continueStoppedSession failed, player not avl")
            return
        }
        //Lets save the player events that were sent earlier. The ones those were pushed earlier, will reappear after deep init of session.
        self.initSession(deep: true)
        setPresentationInformationForContent()
    }

    fileprivate func reportStoppedState() {
        if(currentState != .IDLE && currentState != .STOPPED) {
            if let player = player {
                let time = player.currentTime
                if(time > 0){
                    mmSmartStreaming.reportPlaybackPosition(Int(time) * 1000)
                }
            }
            mmSmartStreaming.report(.STOPPED)
            currentState = .STOPPED
        }
        if (currentAdState != MMAdState.UNKNOWN && currentAdState != .AD_COMPLETED) {
            mmSmartStreaming.report(MMAdState.AD_COMPLETED)
            currentAdState = MMAdState.AD_COMPLETED
        }
    }

    fileprivate func reportUserInitiatedPlayback() {
        mmSmartStreaming.reportUserInitiatedPlayback()
    }

    fileprivate func reportPlayerSeekCompleted(seekEndPos: Int) {
        mmSmartStreaming.reportPlayerSeekCompleted(seekEndPos)
    }
    
    fileprivate func processDuration(changedTime: TimeInterval){
        guard player != nil else {
            return;
        }
        self.presentationDuration = changedTime
        self.setPresentationInformationForContent()
        if let videoQuality = self.player!.videoQuality {
            self.reportPresentationSize(width: Int(videoQuality.width), height: Int(videoQuality.height))
            self.reportDownloadRate(downloadRate: Int(videoQuality.bitrate))
        }
    }
    
    fileprivate func processDurationFromPlayerItem() {
        guard player != nil else {
            return;
        }
        presentationDuration = self.player!.duration
        setPresentationInformationForContent()
    }

    private func setPresentationInformationForContent() {
        guard let contentDuration = presentationDuration else{
            return
        }
        
        guard let player = self.player else {
            return
        }

        if (!presentationInfoSet && !contentDuration.isInfinite) {
            let presentationInfo = MMPresentationInfo()
            if player.isLive {
                presentationInfo.duration = Int(-1)
                presentationInfo.isLive = true
            } else {
                let hasValidDuration = contentDuration != 0
                if(hasValidDuration) {
                    let newDurationSeconds = hasValidDuration ? contentDuration : 0.0
                    BitmovinPlayerIntegrationWrapper.logDebugStatement("Duration of content is \(String(describing: newDurationSeconds * 1000))")
                    let duration  = newDurationSeconds * 1000
                    presentationInfo.duration = Int(duration)
                }
            }
            
            var arrayRepresentations = [MMRepresentation]()
            for videoQuality in self.player!.availableVideoQualities {
                let representation = MMRepresentation()
                representation.bitrate = Int(videoQuality.bitrate)
                representation.width = Int(videoQuality.width)
                representation.height = Int(videoQuality.height)
                representation.codecIdentifier = videoQuality.identifier
                arrayRepresentations.append(representation)
            }
            if (arrayRepresentations.count > 0) {
                //presentationInfo.representations = arrayRepresentations
            }
            mmSmartStreaming.setPresentationInformation(presentationInfo)
            presentationInfoSet = true
        }
    }

    private func addPlayerListener() {
        guard let player = self.player else{
            return
        }
        self.delegate = BitMovinPlayerDelegate(parent: self)
        player.add(listener: self.delegate!)
    }
    
    private func removePlayerListener(player: BitmovinPlayer) {
        if let bmPlayerDelegate = self.delegate {
            player.remove(listener: bmPlayerDelegate)
            self.delegate = nil
        }
    }
    
    fileprivate func fetchAndReportAdInfo(forAd ad: VastAd, adTracker: VastTracker, adIndex: Int) -> MMAdInfo {
        let advertisement = ad
        
        let adInfo = MMAdInfo()
        adInfo.adClient = "Nowtilus"
        adInfo.adId = advertisement.id
        //adInfo.adResolution = "\(advertisement.)x\(advertisement.height)"
        
        switch advertisement.type {
        case .inline:
            adInfo.adType = MMAdType.AD_LINEAR
        default:
            adInfo.adType = MMAdType.AD_UNKNOWN
        }
        
        if let adCreative = advertisement.creatives.first {
            if let linear = adCreative.linear {
                adInfo.adType = MMAdType.AD_LINEAR
                if let duration = linear.duration {
                    adInfo.adDuration = Int(duration * 1000)
                }
            }
        }
        adInfo.adPosition = "mid"
        adInfo.adPodLendth = adTracker.totalAds
        adInfo.adPodIndex = 0
        adInfo.adPositionInPod = adIndex
        //adInfo.adScheduleTime = Int(event.timeOffset)
        self.mmSmartStreaming.report(adInfo)
        return adInfo
    }
}

//MARK:- MMSSAI ADMANAGER DELEGATE
extension BitmovinPlayerIntegrationWrapper: MMSSAIDelegate {
    func notifyAdEventWith(state: MMAdState, vastAd: VastAd?, vastTracker: VastTracker, andAdIndex index: Int) {
        if let adInstance = vastAd {
            let adInfo = self.fetchAndReportAdInfo(forAd: adInstance, adTracker: vastTracker, adIndex: index)
            self.mmssaiDelegate?.notifyAdEventsWith(eventName: state, andAdInfo: adInfo)
        }
        
        switch state {
        case .AD_REQUEST:
            self.mmSmartStreaming.report(MMAdState.AD_REQUEST)
            
        case .AD_IMPRESSION:
            self.isAdBuffering = false
            self.isAdStreaming = true
            self.mmSmartStreaming.reportAdPlaybackTime(0)
            self.mmSmartStreaming.report(MMAdState.AD_IMPRESSION)
            self.currentAdState = MMAdState.AD_IMPRESSION
            
        case .AD_STARTED:
            self.isAdBuffering = false
            self.isAdStreaming = true
            self.mmSmartStreaming.reportAdPlaybackTime(0)
            if (isAdBuffering) {
                self.isAdBuffering = false
            }
            self.mmSmartStreaming.report(MMAdState.AD_STARTED)
            self.currentAdState = MMAdState.AD_STARTED
            
        case .AD_CLICKED:
            self.isAdStreaming = false
            self.mmSmartStreaming.report(MMAdState.AD_CLICKED)
            self.currentAdState = MMAdState.AD_CLICKED
            
        case .AD_COMPLETED:
            self.isAdStreaming = false
            self.mmSmartStreaming.reportAdPlaybackTime(self.adDuration * 1000)
            self.mmSmartStreaming.report(MMAdState.AD_COMPLETED)
            self.currentAdState = MMAdState.AD_COMPLETED
            if (self.isPostRollAd) {
                self.cleanupCurrItem();
            }
            
            
        case .AD_ENDED:
            self.isAdStreaming = false
            //self.mmSmartStreaming.reportAdPlaybackTime(Int(self.adInstance!.playheadTime()) * 1000)
            self.mmSmartStreaming.report(MMAdState.AD_COMPLETED)
            self.currentAdState = MMAdState.AD_ENDED
            if (self.isPostRollAd) {
                self.cleanupCurrItem();
            }
            
        case .AD_PAUSED:
            self.isAdStreaming = true
            //self.mmSmartStreaming.reportAdPlaybackTime(Int(self.adInstance!.playheadTime()) * 1000)
            self.mmSmartStreaming.report(MMAdState.AD_PAUSED)
            self.currentAdState = MMAdState.AD_PAUSED
            
        case .AD_RESUMED:
            self.isAdStreaming = true
            self.mmSmartStreaming.report(MMAdState.AD_RESUMED)
            self.currentAdState = MMAdState.AD_RESUMED
            
        case .AD_SKIPPED:
            self.isAdStreaming = false
            //self.mmSmartStreaming.reportAdPlaybackTime(Int(self.adInstance!.playheadTime()) * 1000)
            self.mmSmartStreaming.report(MMAdState.AD_SKIPPED)
            self.currentAdState = MMAdState.AD_SKIPPED
            
        case .AD_FIRST_QUARTILE:
            self.isAdStreaming = true
            self.mmSmartStreaming.report(MMAdState.AD_FIRST_QUARTILE)
            self.currentAdState = MMAdState.AD_FIRST_QUARTILE
            
        case .AD_MIDPOINT:
            self.isAdStreaming = true
            self.mmSmartStreaming.report(MMAdState.AD_MIDPOINT)
            self.currentAdState = MMAdState.AD_MIDPOINT
            
        case .AD_THIRD_QUARTILE:
            self.isAdStreaming = true
            self.mmSmartStreaming.report(MMAdState.AD_THIRD_QUARTILE)
            self.currentAdState = MMAdState.AD_THIRD_QUARTILE
        
        case .AD_ERROR:
            //self.mmSmartStreaming.reportAdPlaybackTime(Int(self.adInstance!.playheadTime()) * 1000)
            self.mmSmartStreaming.report(MMAdState.AD_ERROR)
            self.currentAdState = MMAdState.AD_ERROR
            
        case .AD_BREAK_STARTED:
            self.mmSmartStreaming.report(MMPlayerState.PAUSED)
            self.currentState = .PAUSED
            
        case .AD_BREAK_COMPLETED:
            self.mmSmartStreaming.report(MMPlayerState.PLAYING)
            self.currentState = .PLAYING
            
        default:
            print("Other events")
        }
    }
}

//MARK:- BITMOVIN PLAYER DELEGATE
private class BitMovinPlayerDelegate: NSObject, PlayerListener {
    private weak var parent: BitmovinPlayerIntegrationWrapper?
    private var adPlaybackTime = 0
    private var timerAdvertisement:Timer?
    private var seekPosition = 0.0
    private var lastSentSeekPosition = 0.0
    private var isAdRequested = false
    private var isAdStreaming = false
    private var isAdBuffering = false
    private var isPostRollAd = false
    
    init(parent: BitmovinPlayerIntegrationWrapper) {
        super.init()
        self.parent = parent
    }
    
    @objc private func updateAdPlaybackTimer() {
        self.adPlaybackTime += 2
    }
    
    private func resetAdvertisementObjects() {
        if let timer = self.timerAdvertisement {
            timer.invalidate()
        }
        self.timerAdvertisement = nil
        self.adPlaybackTime = 0
    }
    /**
    * Is called when the player is ready for immediate playback, because initial audio/video has been downloaded.
    *
    * @param event An object holding specific event data.
    */
    func onReady(_ event: ReadyEvent) {
        self.parent!.processDurationFromPlayerItem()
        //self.parent!.reportUserInitiatedPlayback()
    }
    
    /**
    * Notifies about the intention to start/resume playback.
    *
    * @param event An object holding specific event data.
    */
    func onPlay(_ event: PlayEvent) {
        if !self.parent!.isOnloaded {
            self.parent!.reportUserInitiatedPlayback()
            self.parent!.isOnloaded = true
        } else {
            self.parent!.mmSmartStreaming.report(MMPlayerState.PLAYING)
            self.parent!.currentState = .PLAYING
        }
    }
    
    /**
    * Is called when playback has been started.
    *
    * @param event An object holding specific event data.
    */
    func onPlaying(_ event: PlayingEvent) {
        self.parent!.mmSmartStreaming.report(MMPlayerState.PLAYING)
        self.parent!.currentState = .PLAYING
    }

    /**
    * Is called when the player enters the pause state.
    *
    * @param event An object holding specific event data.
    */
    func onPaused(_ event: PausedEvent) {
        self.parent!.mmSmartStreaming.report(MMPlayerState.PAUSED)
        self.parent!.currentState = .PAUSED
    }

    /**
    * Is called when the current playback time has changed.
    *
    * @param event An object holding specific event data.
    */
    func onTimeChanged(_ event: TimeChangedEvent) {
        if let adManager = self.parent?.mmssaiAdManager {
            adManager.notifyPlayerPlaybackTime(playbackTime: Int(event.currentTime * 1000))
        }
        //self.parent!.processDuration(changedTime: event.currentTime)
    }

    /**
    * Is called when the duration of the current played media has changed.
    *
    * @param event An object holding specific event data.
    */
    func onDurationChanged(_ event: DurationChangedEvent) {
        self.parent!.processDuration(changedTime: event.duration)
    }
    
    /**
    * Is called periodically during seeking.
    *
    * Only applies to VoD streams.
    *
    * @param event An object holding specific event data.
    */
    func onSeek(_ event: SeekEvent) {
        self.seekPosition = Double(event.position)
    }
    
    /**
    * Is called when seeking has been finished and data is available to continue playback.
    *
    * Only applies to VoD streams.
    *
    * @param event An object holding specific event data.
    */
    func onSeeked(_ event: SeekedEvent) {
        if (self.seekPosition != self.lastSentSeekPosition) {
            self.lastSentSeekPosition = self.seekPosition
            self.parent!.reportPlayerSeekCompleted(seekEndPos: Int(self.seekPosition * 1000))
        }
    }
    
    /**
    * Is called periodically during time shifting. Only applies to live streams, please refer to onSeek for VoD streams.
    *
    * @param event An object holding specific event data.
    */
    func onTimeShift(_ event: TimeShiftEvent) {
    }
    
    /**
    * Is called when time shifting has been finished and data is available to continue playback. Only applies to live streams, please refer to onSeeked for VoD streams.
    *
    * @param event An object holding specific event data.
    */
    func onTimeShifted(_ event: TimeShiftedEvent) {
    }
    
    /**
    * Is called when the player is paused or in buffering state and the timeShift offset has exceeded the available timeShift window.
    *
    * @param event An object holding specific event data.
    */
    func onDvrWindowExceeded(_ event: DvrWindowExceededEvent) {
    }
    
    /**
    * Is called when the player begins to stall and to buffer due to an empty buffer.
    *
    * @param event An object holding specific event data.
    */
    func onStallStarted(_ event: StallStartedEvent) {
        self.parent!.reportBufferingStarted()
    }
    
    /**
    * Is called when the player ends stalling, due to enough data in the buffer.
    *
    * @param event An object holding specific event data.
    */
    func onStallEnded(_ event: StallEndedEvent) {
        self.parent!.reportBufferingCompleted()
    }
    
    /**
    * Is called when the current size of the video content has been changed.
    *
    * @param event An object holding specific event data.
    */
    func onVideoSizeChanged(_ event: VideoSizeChangedEvent) {
    }
    
    /**
    * Is called when the current video download quality has changed.
    */
    func onVideoDownloadQualityChanged(_ event: VideoDownloadQualityChangedEvent) {
        var oldBitrate = 0
        var newBitrate = 0
        if let videoQualityOld = event.videoQualityOld {
            oldBitrate = Int(videoQualityOld.bitrate)
        }
        if let videoQualityNew = event.videoQualityNew {
            newBitrate = Int(videoQualityNew.bitrate)
        }
        self.parent!.reportABRSwitch(prevBitrate: oldBitrate, newBitrate: newBitrate)
    }
    
    /**
     * Is called when a web download request has finished.
     */
    func onDownloadFinished(_ event: DownloadFinishedEvent) {
        var redirectedMediaURL: String?
        if let redirectionLocation = event.lastRedirectLocation {
            redirectedMediaURL = redirectionLocation.absoluteString
        }
        if redirectedMediaURL == nil {
            redirectedMediaURL = event.url.absoluteString
        }
        
        BitmovinPlayerIntegrationWrapper.setMediaURLInAssetInfo(urlString: redirectedMediaURL!)
        
        if let adManager = self.parent?.mmssaiAdManager {
            if !adManager.isAdManagerSet {
                adManager.setupMediaURL(mediaURL: redirectedMediaURL!)
            }
        }
    }
    
    /**
    * Is called when the playback of the current media has finished.
    *
    * @param event An object holding specific event data.
    */
    func onPlaybackFinished(_ event: PlaybackFinishedEvent) {
        self.parent!.cleanupCurrItem()
    }
    
    /**
    * Is called when the first frame of the current video is rendered onto the video surface.
    *
    * @param event An object holding specific event data.
    */
    func onRenderFirstFrame(_ event: RenderFirstFrameEvent) {
    }

    /**
    * Is called when an error is encountered.
    *
    * @param event An object holding specific event data.
    */
    func onError(_ event: ErrorEvent) {
        self.parent!.reportError(error: event.message, playbackPosMilliSec: self.parent!.getPlaybackPosition())
    }
    
    /**
    * Is called when a warning occurs.
    *
    * @param event An object holding specific event data.
    */
    func onWarning(_ event: WarningEvent) {
        //self.parent!.reportError(error: event.message, playbackPosMilliSec: self.parent!.getPlaybackPosition())
    }
    
    /**
    * Is called when a new source is loaded. This does not mean that loading of the new manifest has been finished.
    *
    * @param event An object holding specific event data.
    */
    func onSourceLoaded(_ event: SourceLoadedEvent) {
    }
    
    /**
    * Is called when the current source will be unloaded.
    *
    * @param event An object holding specific event data.
    */
    func onSourceWillUnload(_ event: SourceWillUnloadEvent) {
    }
    
    /**
    * Is called when the current source has been unloaded.
    *
    * @param event An object holding specific event data.
    */
    func onSourceUnloaded(_ event: SourceUnloadedEvent) {
    }
    
    /**
    * Is called when the player was destroyed.
    *
    * @param event An object holding specific event data.
    */
    func onDestroy(_ event: DestroyEvent) {
        self.parent!.reportStoppedState()
    }
    
    /**
    * Is called when metadata (i.e. ID3 tags in HLS and EMSG in DASH) are encountered.
    *
    * @param event An object holding specific event data.
    */
    func onMetadata(_ event: MetadataEvent) {
    }
    
    /**
    * Is called when metadata is parsed.
    *
    * @param event An object holding specific event data.
    */
    func onMetadataParsed(_ event: MetadataParsedEvent) {
    }
    
    /**
    * Is called when the cast app is launched successfully.
    *
    * @param event An object holding specific event data.
    */
    func onCastStarted(_ event: CastStartEvent) {
    }
    
    /**
    * Is called when casting is initiated, but the user still needs to choose which device should be used.
    *
    * @param event An object holding specific event data.
    */
    func onCastStart(_ event: CastStartEvent) {
    }
    
    /**
    * Is called when the playback on an cast device has been paused.
    *
    * @param event An object holding specific event data.
    */
    func onCastPaused(_ event: CastPausedEvent) {
    }
    
    /**
    * Is called when the playback on an cast device has been finished.
    *
    * @param event An object holding specific event data.
    */
    func onCastPlaybackFinished(_ event: CastPlaybackFinishedEvent) {
    }
    
    /**
    * Is called when playback on an cast device has been started.
    *
    * @param event An object holding specific event data.
    */
    func onCastPlaying(_ event: CastPlayingEvent) {
    }
    
    /**
    * Is called when the cast app is launched successfully.
    *
    * @param event An object holding specific event data.
    */
    func onCastStarted(_ event: CastStartedEvent) {
    }
    
    /**
    * Is called when casting to a device is stopped.
    *
    * @param event An object holding specific event data.
    */
    func onCastStopped(_ event: CastStoppedEvent) {
    }

    /**
     * Is called when the playback of an ad has been started.
     */
    func onAdStarted(_ event: AdStartedEvent) {
        //self.parent!.fetchAndReportAdInfo(forEvent: event)
        self.isAdStreaming = true
        self.parent!.mmSmartStreaming.reportAdPlaybackTime(0)
        if (self.isAdBuffering) {
            self.isAdBuffering = false
        }
        
        self.parent!.mmSmartStreaming.report(MMAdState.AD_IMPRESSION)
        self.parent!.currentAdState = MMAdState.AD_IMPRESSION
        
        //self.parent!.fetchAndReportAdInfo(forEvent: event)
        self.parent!.mmSmartStreaming.report(MMAdState.AD_STARTED)
        self.parent!.currentAdState = MMAdState.AD_STARTED
        
        self.resetAdvertisementObjects()
        self.timerAdvertisement = Timer.scheduledTimer(timeInterval: self.parent!.TIME_INCREMENT, target:self, selector:#selector(self.updateAdPlaybackTimer), userInfo: nil, repeats: true)
        
        if let adPosition = event.position {
            switch adPosition {
            case "pre":
                self.isPostRollAd = false
            case "mid":
                self.isPostRollAd = false
            case "post":
                self.isPostRollAd = true
            default:
                print("Invalid ad position")
            }
        }
    }

    /**
     * Is called when the playback of an ad has been finished.
     */
    func onAdFinished(_ event: AdFinishedEvent) {
        self.isAdStreaming = false
        self.parent!.mmSmartStreaming.report(MMAdState.AD_COMPLETED)
        self.parent!.currentAdState = MMAdState.AD_COMPLETED
        self.resetAdvertisementObjects()
        //Check and unmark
        if (self.isPostRollAd) {
            self.parent!.cleanupCurrItem()
        }
    }

    /**
     * Is fired when the playback of an ad has progressed over a quartile boundary.
     */
    func onAdQuartile(_ event: AdQuartileEvent) {
        self.isAdStreaming = true
        switch event.adQuartile {
        case AdQuartile.firstQuartile:
            self.parent!.mmSmartStreaming.report(MMAdState.AD_FIRST_QUARTILE)
            self.parent!.currentAdState = MMAdState.AD_FIRST_QUARTILE
        case AdQuartile.thirdQuartile:
            self.isAdStreaming = true
            self.parent!.mmSmartStreaming.report(MMAdState.AD_THIRD_QUARTILE)
            self.parent!.currentAdState = MMAdState.AD_THIRD_QUARTILE
        default:
            self.parent!.mmSmartStreaming.report(MMAdState.AD_MIDPOINT)
            self.parent!.currentAdState = MMAdState.AD_MIDPOINT
        }
    }

    /**
     * Is called when the playback of an ad break has been started
     */
    func onAdBreakStarted(_ event: AdBreakStartedEvent) {
    }

    /**
     * Is called when the playback of an ad break has been finished.
     */
    func onAdBreakFinished(_ event: AdBreakFinishedEvent) {
    }

    /**
     * Is called when an ad manifest was successfully downloaded and parsed and the ad has been added onto the queue.
     */
    func onAdScheduled(_ event: AdScheduledEvent) {
    }

    /**
     * Is called when an ad has been skipped.
     */
    func onAdSkipped(_ event: AdSkippedEvent) {
        self.isAdStreaming = false
        self.parent!.mmSmartStreaming.report(MMAdState.AD_SKIPPED)
        self.parent!.currentAdState = MMAdState.AD_SKIPPED
        self.resetAdvertisementObjects()
    }

    /**
     * Is called when the user clicks on the ad.
     */
    func onAdClicked(_ event: AdClickedEvent) {
        self.isAdStreaming = false
        self.parent!.mmSmartStreaming.report(MMAdState.AD_CLICKED)
        self.parent!.currentAdState = MMAdState.AD_CLICKED
    }

    /**
     * Is called when ad playback fails.
     */
    func onAdError(_ event: AdErrorEvent) {
        self.parent!.mmSmartStreaming.reportError(event.message, atPosition: 0)
        self.resetAdvertisementObjects()
    }

    /**
     * Is called when the download of an ad manifest is starting
     */
    func onAdManifestLoad(_ event: AdManifestLoadEvent) {
    }

    /**
     * Is called when the ad manifest has been successfully loaded.
     */
    func onAdManifestLoaded(_ event: AdManifestLoadedEvent) {
        if !self.isAdRequested {
            self.isAdRequested = true
            self.parent!.mmSmartStreaming.report(MMAdState.AD_REQUEST)
        }
    }
    
    func onEvent(_ event: PlayerEvent) {
    }
}
