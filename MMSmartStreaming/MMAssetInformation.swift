//
//  MMAssetInformation.swift
//  AllInOneDemoPlayer
//
//  Created by MacAir 1 on 08/10/20.
//  Copyright © 2020 FreeWheel. All rights reserved.
//

import UIKit
import Foundation
import MMSmartStreamingPrivate

@objc public class MMAssetInformation: NSObject {
    //MARK:- OBJECTS
    public var assetURL: String! //URL of the Asset
    public var assetID: String? //optional identifier of the asset
    public var assetName: String? //optional name of the asset
    public var videoId: String? //optional identifier of the asset group (or) sub asset
    public var qbrMode: MMQBRMode = MMQBRMode.QBRModeDisabled //Needed only when QBR is to be integrated
    public var metafileURL: URL? //Needed only when QBR is to be integrated
    public var customKVPs: [String:String]! //Custom Metadata of the asset
    
    //MARK:- METHODS
    /*
     * Creates an object to hold information identifying the asset that is to be played back on the Player
     * User must specify URL of the asset.
     * User may optionally specify the identifier identifying the asset, its name and collection to which this asset belongs
     */
    @objc public init(assetURL aURL: String, assetID aId: String?, assetName aName: String?, videoId vId: String?) {
        self.assetURL = aURL
        
        if let aId = aId {
            self.assetID = aId
        }
        
        if let aName = aName {
            self.assetName = aName
        }
        
        if let vId = vId {
            self.videoId = vId
        }
        
        self.customKVPs = [:]
    }
    
    /*
     * Lets user specify the custom metadata to be associated with the asset, for example - Genre, DRM etc
     *
     * Call to this API is optional
     */
    @objc public func addCustomKVP(_ key: String, _ value: String) {
        self.customKVPs[key] = value
    }
    
    /*
     * Sets the mode to be used for QBR and the meta file url from where content metadata can be had.
     * Meta file URL is to be provided only if metadata cant be had on the default location.
     *
     * Please note that call to this method is needed only if QBR is integrated to the player.
     */
    @objc public func setQBRMode(_ mode: MMQBRMode, withMetaURL metaURL: URL?) { //Needed only when QBR is to be integrated
        self.qbrMode = mode
        if let metaURL = metaURL {
            self.metafileURL = metaURL
        }
    }
}
